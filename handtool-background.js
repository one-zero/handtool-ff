function unzoomMessage(request, sender, sendResponse) {
    if(request.zoom == "Unzoom") {
        var setting = browser.tabs.setZoom(1);
    }
}

function handleZoomed(zoomChangeInfo) {
        if (zoomChangeInfo.newZoomFactor > 1) {
            browser.tabs.sendMessage(zoomChangeInfo.tabId, {zoomStatus: "zoomed"});
        }
}

browser.runtime.onMessage.addListener(unzoomMessage);
browser.tabs.onZoomChange.addListener(handleZoomed);
