if (document.body.childNodes.length == 1 && document.body.childNodes[0].nodeName == "IMG") {
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = 'img { pointer-events: all; } div { position:fixed; width: 100%; height: 100%; overflow: auto}';
    document.getElementsByTagName('head')[0].appendChild(style);

    var el = document.querySelector('img');
    var wrapper = document.createElement('div');
    el.parentNode.insertBefore(wrapper, el);
    wrapper.appendChild(el);

    document.querySelector("title").nodeValue = document.querySelector("title").innerHTML.replace(/ - Scaled \([0-9]{1,3}\%\)/, "")
    

    /* Flags */
    var handtoolIsSpaced = null;
    var handtoolIsClicked = null;
    var handtoolIsMoved = null;

    /* Coordinate variables */
    var handtoolInitX = 0;
    var handtoolInitY = 0;
    var handtoolMovedX = 0;
    var handtoolMovedY = 0
    var handtoolVelocity = 0;
    var handtoolOffsetX = 0;
    var handtoolOffsetY = 0;

    /* Position update interval variable */
    var handtoolUpdateLoop = null;

    /* Zoom variables */
    el = document.querySelector('img');
    var handtoolImgWidth = el.width;
    var handtoolImgHeight = el.height;

    var handtoolIsZoomable = false;
    var handtoolIsZoomed = false;

    if(el.naturalWidth > el.clientWidth || el.naturalHeight > el.clientHeight) {
        wrapper.style.cursor = "zoom-in";
        handtoolIsZoomable = true;
    }


    function handtoolGetVelocity() {
        velocity = startPosition - gesturesY;
    }

    function handtoolSpaceDown(e) {
        if (e.keyCode == 32) {
            e.preventDefault();
            if(handtoolIsClicked) {
                wrapper.style.cursor = "grabbing";
            } else {
                wrapper.style.cursor = "grab";
            }
            handtoolIsSpaced = true;
            el.style.pointerEvents = "none";
        }
    }

    function handtoolSpaceUp(e) {
        if (e.keyCode == 32) {
            if(!handtoolIsZoomed) {
                if(handtoolIsZoomable) {
                    wrapper.style.cursor = "zoom-in";
                }
                else {
                    wrapper.style.cursor = "auto";
                }
            }
            else {
                wrapper.style.cursor = "zoom-out";
            }
            handtoolIsSpaced = false;
            window.clearInterval(handtoolUpdateLoop);
            handtoolUpdateLoop = null;
            el.style.pointerEvents = "all";
        }
    }

    function handtoolMouseDown(e) {
        e.preventDefault();
        handtoolIsClicked = true;
        // wrapper.style.cursor = "grabbing";
        handtoolInitX = e.clientX;
        handtoolInitY = e.clientY;
    }

    function handtoolMouseUp(e) {
        e.preventDefault(e);
        e.stopImmediatePropagation(e);
        handtoolIsClicked = false;
        window.clearInterval(handtoolUpdateLoop);
        handtoolUpdateLoop = null;
    }

    function handtoolPositionUpdate() {
        handtoolInitX = handtoolMovedX;
        handtoolInitY = handtoolMovedY;
    }

    function handtoolMouseMove(e) {
        handtoolMovedX = parseInt(e.pageX, 10);
        handtoolMovedY = parseInt(e.pageY, 10);
        handtoolOffsetX = Math.floor((handtoolInitX - handtoolMovedX) / 2);
        handtoolOffsetY = Math.floor((handtoolInitY - handtoolMovedY) / 2);
        if (handtoolIsClicked && handtoolIsSpaced) {
            handtoolIsMoved = true;
            e.preventDefault();
            wrapper.scrollBy(handtoolOffsetX, handtoolOffsetY);
            if(!handtoolUpdateLoop) {
                handtoolUpdateLoop = window.setInterval(handtoolPositionUpdate, 30);
            }
        }
    }

    function handtoolToggleZoom(e) {
        if (handtoolIsMoved) {
            e.stopImmediatePropagation();
            handtoolIsMoved = false;
            el.style.pointerEvents = "all";
            return;
        }
        if(!handtoolIsSpaced && e.button == 0) {
            if(handtoolIsZoomed) {
                el.width = handtoolImgWidth;
                el.height = handtoolImgHeight;
                el.classList.remove("overflowingVertical");
                el.classList.add("shrinkToFit");
                var handtoolsendMsg = browser.runtime.sendMessage({
                    zoom: "Unzoom"
                });
                handtoolIsZoomed = false;
                if(handtoolIsZoomable) {
                    wrapper.style.cursor = "zoom-in";
                }
                else {
                    wrapper.style.cursor = "auto";
                }
            }
            else if (handtoolIsZoomable && !handtoolIsZoomed) {
                var windowWidth = window.innerWidth;
                var windowHeight = window.innerHeight;
                var imageWidthUnzoomed = el.clientWidth;
                var imageHeightUnzoomed = el.clientHeight;

                var differenceLeftRight = Math.ceil((windowWidth - imageWidthUnzoomed) / 2);
                var differenceTopBottom = Math.ceil((windowHeight - imageHeightUnzoomed) / 2);

                var scrollToX;
                var scrollToY;
                if (e.clientX <= differenceLeftRight) {
                    scrollToX = 0;
                }
                else {
                    scrollToX = (e.clientX - differenceLeftRight) / imageWidthUnzoomed;
                }
                if (e.clientY <= differenceTopBottom) {
                    scrollToY = 0;
                }
                else {
                    scrollToY = (e.clientY - differenceTopBottom) / imageHeightUnzoomed;
                }
                el.removeAttribute("width");
                el.removeAttribute("height");
                wrapper.style.cursor = "zoom-out";
                el.classList.remove("shrinkToFit");
                el.classList.add("overflowingVertical");

                scrollToPosX = Math.floor((el.clientWidth * scrollToX) - (windowWidth * scrollToX));
                scrollToPosY = Math.floor((el.clientHeight * scrollToY) - (windowHeight * scrollToY));
                wrapper.scrollTo(scrollToPosX, scrollToPosY);
                console.log(scrollToPosX, " - ", scrollToPosY);
                handtoolIsZoomed = true;
                wrapper.style.cursor = "zoom-out";
            }
        }
    }

    function handtoolZoomChange(request, sender, sendResponse) {
        if(request.zoomStatus == "zoomed") {
            handtoolIsZoomed = true;
            wrapper.style.cursor = "zoom-out";
        }
    }

    window.addEventListener("keydown", handtoolSpaceDown);
    window.addEventListener("keyup", handtoolSpaceUp);

    window.addEventListener("click", handtoolToggleZoom, true);

    window.addEventListener("mousedown", function(event) {
        if (event.button === 0) {
            handtoolMouseDown(event);
        }
    });

    window.addEventListener("contextmenu", function(event) {
        if (handtoolIsSpaced) {
            event.preventDefault();
        }
    });


    window.addEventListener("mouseup", handtoolMouseUp);

    window.addEventListener("mousemove", handtoolMouseMove);

    browser.runtime.onMessage.addListener(handtoolZoomChange);
}
